﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inverse_kinematics
{
    class Fabrik2D
    {

        private struct JOIN
        {
            public double x; // x position of joint relative to origin
            public double y; // y position of joint relative to origin
            public double angle; // angle of joint (if the joint has adjacent joints or origin)
        }
    
    // Chain struct
        private struct CHAIN
        {
            public JOIN[] joints; // list of joints
            public double z;  // z position defining the offset of the chain from the plane
            public double angle; // base (plane) rotation
        }
    
        // Number of joints in the chain
        private int numJoints;

        // Tolerance of distance between end effector and target
        private double tolerance;

        private CHAIN chain;



        private void createChain(int[] lengths)
        {
            chain.joints = new JOIN[this.numJoints]; 

            chain.joints[0].x = 0;
            chain.joints[0].y = 0;
            chain.joints[0].angle = 0;

            int sumLengths = 0;
            for (int i = 1; i < this.numJoints; i++)
            {
                sumLengths = sumLengths + lengths[i - 1];
                chain.joints[i].x = 0;
                chain.joints[i].y = sumLengths;
                chain.joints[i].angle = 0;
            }
        }


        private double distance(double x1, double y1, double x2, double y2)
        {
            double xDiff = x2 - x1;
            double yDiff = y2 - y1;
            return Math.Sqrt(xDiff * xDiff + yDiff * yDiff);
        }






        public Fabrik2D(int numJs, int[] lengths)
        {
            this.numJoints = numJs;
            createChain(lengths);

            this.tolerance = 1; // 1mm tolerance default
        }





        public bool solve(double x, double y, int[] lengths)
        {
            // Distance between root and target (root is always 0,0)
            int dist = (int) Math.Sqrt(x * x + y * y);

            // Total length of chain
            int totalLength = 0;
            for (int i = 0; i < this.numJoints - 1; i++)
            {
                totalLength = totalLength + lengths[i];
            }

            // Check whether the target is within reach
            if (dist > totalLength)
            {
                // The target is unreachable

                for (int i = 0; i < this.numJoints - 1; i++)
                {
                    // Find the distance r_i between the target (x,y) and the joint i position (jx,jy)
                    double jx = this.chain.joints[i].x;
                    double jy = this.chain.joints[i].y;
                    double r_i = distance(jx, jy, x, y);
                    double lambda_i = ((double)lengths[i]) / r_i;

                    // Find the new joint positions
                    this.chain.joints[i + 1].x = (double)((1 - lambda_i) * jx + lambda_i * x);
                    this.chain.joints[i + 1].y = (double)((1 - lambda_i) * jy + lambda_i * y);
                }

                return false;
            }
            else
            {
                // The target is reachable; this, set as (bx,by) the initial position of the joint i
                double bx = this.chain.joints[0].x;
                double by = this.chain.joints[0].y;

                // Check whether the distance between the end effector joint n (ex,ey) and the target is 
                // greater than a tolerance
                double ex = this.chain.joints[this.numJoints - 1].x;
                double ey = this.chain.joints[this.numJoints - 1].y;
                double dif = distance(ex, ey, x, y);

                double prevDif = 0;
                double tolerance = this.tolerance;
                while (dif > tolerance)
                {
                    if (prevDif == dif)
                        tolerance *= 2;

                    prevDif = dif;

                    // STAGE 1: FORWARD REACHING
                    // Set the end effector as target
                    this.chain.joints[this.numJoints - 1].x = x;
                    this.chain.joints[this.numJoints - 1].y = y;

                    for (int i = this.numJoints - 2; i >= 0; i--)
                    {
                        // Find the distance r_i between the new joint position i+1 (nx,ny)
                        // and the joint i (jx,jy)
                        double jx = this.chain.joints[i].x;
                        double jy = this.chain.joints[i].y;
                        double nx = this.chain.joints[i + 1].x;
                        double ny = this.chain.joints[i + 1].y;
                        double r_i = distance(jx, jy, nx, ny);
                        double lambda_i = ((double)lengths[i]) / r_i;

                        // Find the new joint positions
                        this.chain.joints[i].x = (double)((1 - lambda_i) * nx + lambda_i * jx);
                        this.chain.joints[i].y = (double)((1 - lambda_i) * ny + lambda_i * jy);
                    }

                    // STAGE 2: BACKWARD REACHING
                    // Set the root at its initial position
                    this.chain.joints[0].x = bx;
                    this.chain.joints[0].y = by;

                    for (int i = 0; i < this.numJoints - 1; i++)
                    {
                        // Find the distance r_i between the new joint position i (nx,ny)
                        // and the joint i+1 (jx,jy)
                        double jx = this.chain.joints[i + 1].x;
                        double jy = this.chain.joints[i + 1].y;
                        double nx = this.chain.joints[i].x;
                        double ny = this.chain.joints[i].y;
                        double r_i = distance(jx, jy, nx, ny);
                        double lambda_i = ((double)lengths[i]) / r_i;

                        // Find the new joint positions
                        this.chain.joints[i + 1].x = (double)((1 - lambda_i) * nx + lambda_i * jx);
                        this.chain.joints[i + 1].y = (double)((1 - lambda_i) * ny + lambda_i * jy);
                    }

                    // Update distance between end effector and target
                    ex = this.chain.joints[this.numJoints - 1].x;
                    ey = this.chain.joints[this.numJoints - 1].y;
                    dif = distance(ex, ey, x, y);
                }
            }


            this.chain.joints[0].angle = Math.Atan2(this.chain.joints[1].y, this.chain.joints[1].x);

            double prevAngle = this.chain.joints[0].angle;
            for (int i = 2; i <= this.numJoints - 1; i++)
            {
                double ax = this.chain.joints[i - 1].x;
                double ay = this.chain.joints[i - 1].y;
                double bx = this.chain.joints[i].x;
                double by = this.chain.joints[i].y;

                double aAngle = Math.Atan2(by - ay, bx - ax);

                this.chain.joints[i - 1].angle = aAngle - prevAngle;

                prevAngle = aAngle;
            }

            return true;
        }




        public bool solve(float x, float y, float toolAngle, int[] lengths)
        {
            return solve2(x, y, 0, toolAngle, 0, lengths);
        }


        public bool solve(float x, float y, float toolAngle, float grippingOffset, int[] lengths)
        {
            return solve2(x, y, 0, toolAngle, grippingOffset, lengths);
     
        }


        public bool solve2(float x, float y, float z, int[] lengths)
        {
            double r = distance(0, 0, x, z);

            bool solvable = solve(r, y, lengths);
            if (solvable == true)
            {
                this.chain.z = z;
                this.chain.angle = Math.Atan2(z, x);
            }

            return solvable;
        }


        public bool solve2(float x, float y, float z, float toolAngle, int[] lengths)
        {
            return solve2(x, y, z, toolAngle, 0, lengths);
        }


        public bool solve2(float x, float y, float z, float toolAngle, float grippingOffset, int[] lengths)
        {
            if (this.numJoints >= 4)
            {

                // Find wrist center by moving from the desired position with tool angle and link length
                double oc_x = x - (lengths[this.numJoints - 2] + grippingOffset) * Math.Cos(toolAngle);
                double oc_y = y - (lengths[this.numJoints - 2] + grippingOffset) * Math.Sin(toolAngle);

                // We solve IK from first joint to wrist center
                int tmp = this.numJoints;
                this.numJoints = this.numJoints - 1;

                bool solvable = solve(oc_x, oc_y, lengths);

                this.numJoints = tmp;

                if (solvable == true)
                {
                    // Update the end effector position to preserve tool angle
                    this.chain.joints[this.numJoints - 1].x =  this.chain.joints[this.numJoints - 2].x + lengths[this.numJoints - 2] * Math.Cos(toolAngle);
                    this.chain.joints[this.numJoints - 1].y =  this.chain.joints[this.numJoints - 2].y + lengths[this.numJoints - 2] * Math.Sin(toolAngle);

                    // Update angle of last joint
                    this.chain.joints[0].angle = Math.Atan2(this.chain.joints[1].y, this.chain.joints[1].x);

                    double prevAngle = this.chain.joints[0].angle;
                    for (int i = 2; i <= this.numJoints - 1; i++)
                    {
                        double ax = this.chain.joints[i - 1].x;
                        double ay = this.chain.joints[i - 1].y;
                        double bx = this.chain.joints[i].x;
                        double by = this.chain.joints[i].y;

                        double aAngle = Math.Atan2(by - ay, bx - ax);

                        this.chain.joints[i - 1].angle = aAngle - prevAngle;

                        prevAngle = aAngle;
                    }

                    // Save tool angle
                    this.chain.joints[this.numJoints - 1].angle = toolAngle;

                    // Save base angle (if z different from zero)
                    if (z != 0)
                    {
                        this.chain.z = z;
                        this.chain.angle = Math.Atan2(z, x);
                    }

                }

            }

            return true;
        }



        public double getX(int joint)
        {
            if (joint >= 0 && joint < numJoints)
            {
                return this.chain.joints[joint].x;
            }
            return 0;
        }


        public double getY(int joint)
        {
            if (joint >= 0 && joint < numJoints)
            {
                return this.chain.joints[joint].y;
            }
            return 0;
        }


        public double getZ()
        {
            return this.chain.z;
        }


        public double getAngle(int joint)
        {
            if (joint >= 0 && joint < numJoints)
            {
                return this.chain.joints[joint].angle;
            }
            return 0;
        }


        public double getBaseAngle()
        {
            return this.chain.angle;
        }


        public void setBaseAngle(double baseAngle)
        {
            this.chain.angle = baseAngle;
        }


        public void setTolerance(double tolerance)
        {
            this.tolerance = tolerance;
        }


        public void setJoints(double[] angles, int[] lengths)
        {
            // int angListLen = sizeof(angles) / sizeof(float);
            //int lenListLen = sizeof(lengths) / sizeof(float);
            int angListLen = angles.Length;
            int lenListLen = lengths.Length; 

            if (angListLen == numJoints && lenListLen == numJoints - 1)
            {
                double accAng = angles[0];
                double accX = 0;
                double accY = 0;
                this.chain.joints[0].angle = angles[0];

                for (int i = 1; i < this.numJoints; i++)
                {
                    accAng += angles[i];
                    this.chain.joints[i].x = accX + lengths[i - 1] * Math.Cos(accAng);
                    this.chain.joints[i].y = accY + lengths[i - 1] * Math.Sin(accAng);
                    this.chain.joints[i].angle = angles[i];
                    accX = this.chain.joints[i].x;
                    accY = this.chain.joints[i].y;
                }
            }
        }


        public int getNumOfLenghs()
        {
            return this.numJoints;
        }




    }
}
