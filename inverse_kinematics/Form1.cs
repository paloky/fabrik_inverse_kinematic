﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.Collections;


namespace inverse_kinematics
{
    public partial class Form1 : Form
    {

        Fabrik2D fabrik2D;
        int[] lengths;

        // Offset del Punt Base (Root)
        Point base_offset = new Point();

        bool drag = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // For a 2DOF arm, we have 2 links and 2+1 joints, 
            // where the end effector counts as one joint in this case.
            lengths = new int[]{ 225, 150, 50 }; // Length of shoulder and elbow in mm.
            fabrik2D = new Fabrik2D(4, lengths); // 3 Joints in total

            // Tolerance determines how much error is allowed for solving
            // the inverse kinematics for the end effector to reach the
            // desired point.
            fabrik2D.setTolerance(0.5);

            // Solve IK, move up to x=200, y=50
            fabrik2D.solve(200, 50, lengths);

            // Offset del punt base
            base_offset.X = 100;
            base_offset.Y = 100;
        }


        private void Button1_Click(object sender, EventArgs e)
        {
            int x_pos = Convert.ToInt16(tb_Xpos.Text);
            int y_pos = Convert.ToInt16(tb_Ypos.Text);

            // Solve IK, move up to x=200, y=50
            fabrik2D.solve(x_pos, y_pos, lengths);


            // Get the angles (in radians [-pi,pi]) and convert them to degrees [-180,180]
            double shoulderAngle = fabrik2D.getAngle(0) * 57296 / 1000; // In degrees
            double elbowAngle = fabrik2D.getAngle(1) * 57296 / 1000; // In degrees

            panel1.Invalidate();
        }



        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = panel1.CreateGraphics();

            // Color de les linies
            Pen p = new Pen(Color.Black);

            // Color del Node
            SolidBrush sb = new SolidBrush(Color.Black);

            // Diametre del Node
            int mida_unio = 16;

            // Pintem el Node Root
            int OrigenX = (int)fabrik2D.getX(0);
            int OrigenY = (int)fabrik2D.getY(0);

            OrigenX += base_offset.X;
            OrigenY += base_offset.Y;
                
            g.DrawEllipse(p, OrigenX - (mida_unio / 2), OrigenX - (mida_unio / 2), mida_unio, mida_unio);
            g.FillEllipse(sb, OrigenX - (mida_unio / 2), OrigenX - (mida_unio / 2), mida_unio, mida_unio);


            sb = new SolidBrush(Color.DarkRed);
            for (int i=0; i<this.lengths.Length; i++)
            {
                // Punt d'origen
                OrigenX = (int)fabrik2D.getX(i);
                OrigenY = (int)fabrik2D.getY(i);
                OrigenX += base_offset.X;
                OrigenY += base_offset.Y;

                // Punt desti
                int DestiX = (int)fabrik2D.getX(i+1);
                int DestiY = (int)fabrik2D.getY(i+1);
                DestiX += base_offset.X;
                DestiY += base_offset.Y;

                // Dibuixar la Linea
                g.DrawLine(p, new Point(OrigenX, OrigenY), new Point(DestiX, DestiY));

                // Si es l'ultim node, li canviem el color
                if (i==this.lengths.Length-1) sb = new SolidBrush(Color.Green);
                
                // Dibuixa la Unio
                g.DrawEllipse(p, DestiX-(mida_unio / 2), DestiY-(mida_unio / 2), mida_unio, mida_unio);
                g.FillEllipse(sb, DestiX-(mida_unio / 2), DestiY-(mida_unio / 2), mida_unio, mida_unio);

            }

        }

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            drag = true;
           
        }

        private void Panel1_MouseUp(object sender, MouseEventArgs e)
        {
            drag = false;
        }

        private void Panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (drag)
            {
                //Buscar si estem pulsant el Join Final
                Point p = new Point();

                //Obtenim la posicio del ultim Join
                p.X = (int)fabrik2D.getX(this.lengths.Length);
                p.Y = (int)fabrik2D.getY(this.lengths.Length);
                p.X += base_offset.X;
                p.Y += base_offset.Y;

                if (e.Location.X > p.X - 8 && e.Location.X < p.X + 8)
                {
                    if (e.Location.Y > p.Y - 8 && e.Location.Y < p.Y + 8)
                    {
                        bool trobat = true;
                        // Posicionem a la posicio del cursor
                        int newX = e.Location.X - base_offset.X;
                        int newY = e.Location.Y - base_offset.Y;
                        fabrik2D.solve(newX, newY, lengths);
                        panel1.Invalidate();
                    }
                }
            }
        }
    }
}
